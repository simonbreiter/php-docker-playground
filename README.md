# README

This repository is intended to be simple playground for php on docker or as a 
starting point for a new project or sketch.

## How to use

Build and start container:
```bash
./up
```

Shutdown container:
```bash
./down
```

Put your PHP files into `/web` and have fun!